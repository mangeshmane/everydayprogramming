import java.util.Scanner;
class RunIf{
	private int id;
	
	private String name;
	
	private String addr;
	
	public RunIf(int i, String string, String string2) {
		this.id = i;
		this.name = string;
		this.addr = string2;
	}
	public RunIf() {
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	@Override
	public String toString() {
		return "RunIf [id=" + id + ", name=" + name + ", addr=" + addr + "]";
	}
	
}
class Mandrove{
	private int id;
	private String name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public RunIf filterRunIf(RunIf ri) {
		if(ri.getName().isEmpty() || ri.getName().equals(null)) {
			RunIf rir = new RunIf();
			return rir;
		}
		else if(ri.getAddr().equals(null))
			return new RunIf();
		else
			return new RunIf(1,"asdklf","sldfkj");
	}
}
public class PracticeJava {

	public static void main(String[] args) {
			
		RunIf ri = new RunIf();
		ri.setAddr("adsf");
		ri.setId(1);
		ri.setName("sa;dfh");
		
		Mandrove mn = new Mandrove();
		RunIf rri = mn.filterRunIf(ri);
		System.out.println("rri is equal to : "+ rri);
		
		Scanner in = new Scanner(System.in);
		in.hasNext();
        int t=in.nextInt();
        for(int i=0;i<t;i++){
            int r = 0;
            int a = in.nextInt();
            int b = in.nextInt();
            int n = in.nextInt();
            for(int j = 1;j<=n;j++){
                System.out.print(r+" ");
                r += ((int)Math.pow(2,j)*b);
                
            }
            System.out.println();
        } 
		
		int i, m = 0, flag = 0;
		int n = 3;// it is the number to be checked
		m = n / 2;
		if (n != 0 || n != 1 || n % 2 != 0) {
			System.out.println(n + " is not prime number");
		} else {
			for (i = 2; i <= m; i++) {
				if (n % i == 0) {
					System.out.println(n + " is not prime number");
					flag = 1;
					break;
				}
			}
			if (flag == 0) {
				System.out.println(n + " is prime number");
			}
		} // end of else
		int ab = 0;
		for (int ij = 0; ij < 100; ij++) {
			if (ij != 0 || ij != 1 || ij % 2 != 0) {

				ab = ab + ij;

			}
		}
		System.out.println("sum of 100 prime numberis " + ab);
	}
}
