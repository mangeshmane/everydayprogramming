package com.demo.filep.practice;

public class StaticInterfaceImpl implements StaticInterface {

	public void show() {
	}

	public static void main(String[] args) {
		StaticInterface.random();
		int no = 1234;
		int temp = 0;
		while (no > 0) {
			temp = temp * 10 + no % 10;
			no = no / 10;
		}
		System.out.println(temp);
	}
}
