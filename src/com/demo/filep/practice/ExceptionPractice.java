package com.demo.filep.practice;

import java.util.Scanner;
interface Abc{
	public int getAge();  //here we created the interface and declare the method , so this method needs to 
	                      //implements somewhere but we are not gonna do that we just creating the java annonymous 
						  // class and implement the method in it and also learn about the lambda function .
}

public class ExceptionPractice {

	public static void main(String[] args) {
		//lambda and annonymous class
		
		Abc annonymous = () -> {
			return 23;
			
			//			public int getAge() {
//				System.out.println("you are now at correct way");
//				return 323;
//			}   //instead of this annonymous class we will write the lambda expression

		};
		
		Scanner sc = new Scanner(System.in);
		System.out.println("please enter your age");
		int age = sc.nextInt();
		
		try {
			
			if (age < 18) {
				throw new AgeException("Your age is not applicable to vote");
			}

		} catch (RuntimeException e) {
			System.out.println(e.getMessage().toString());
			e.printStackTrace();
		}
		System.out.println("Hey bro .....");
		AgeException radfs = new AgeException();
		System.out.println(radfs.arrrr);
	}
	
	
}


class AgeException extends RuntimeException {
	int arrrr = 20;
	public AgeException() {}
	public AgeException(String info) {
		super(info);
	}
}