package com.demo.filep.practice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.BiConsumer;
import java.util.Scanner;
import java.util.Set;

public class Test {

	public static void m1(String abc) {
		System.out.println("abc");
	}

	public static void m1(Integer abc) {
		System.out.println("abcd");
	}

	public static void main(String[] args) {
		int prime = 2, count = 0; // we start a for loop from 2
		for (int i = 2; i <= prime; i++) { // here we have to check the number 20 is divisible by 1 to 20 numbers
			if (prime % i == 0) { // if divisible by rather than one and 20 then its not a prime number
				count += 1;
			}
		}
		if (count > 0) {
			System.out.println("number is not prime");
		} else
			System.out.println("number is prime");
		int primecounter = 0;
		for (int i = 1; i <= 100; i++) {
			int counter = 0;
			for (int j = 2; j < i; j++) {
				if (i % j == 0) {
					counter += 1;
				}
			}
			if (counter == 0) {
				System.out.println(i + "is prime number");
				primecounter++;
			}
			else
				counter = 0;
		}
		System.out.println("total prime between 1 to 100 is: "+primecounter);
		///////////////////////////////////////////////////////////////////////////////////////////////
		Scanner sc = new Scanner(System.in);
		System.out.println("printing the pattern in java");
		// Rectangle triangle1
		System.out.println("enter the height of the triangle:");
		int height = sc.nextInt();
		for (int i = 0; i < height; i++) {
			for (int j = 0; j <= i; j++) { // this is for rectangle triangle.
				System.out.print("*"); // lower to higher
			}
			System.out.println("");
		}
		System.out.println("======================");
		for (int i = 0; i < height; i++) {
			for (int j = height; j > i; j--) { // higher to lower
				System.out.print("*");
			}
			System.out.println("");
		}
//		
//		for(int i = 0 ;i<8;i++) {
//			for(int j = 8-i;j>1;j--) {
//				System.out.print(" ");
//			}
//			for(int j = 0;j<=i;j++) {
//				System.out.print("* ");
//			}
//			System.out.println("");
//		}

		// end of the pattern problems.

//////////////////////////////////////////////////////////////////////////////////////////////		
		System.out.println("======= array practice ========");
		// 2d Array practice
		int[][] ar = new int[2][3]; // this is the 2row and 3 column array.
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 3; j++) { // array initialization
				ar[i][j] = sc.nextInt();
			}
		}
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 3; j++) { // arrary printing
				System.out.print(ar[i][j] + " ");
			}
		}
		System.out.println("\n");
		// calculte the follwing count and with add array print array from user
		int a = 3 / 2;
		System.out.println("a:" + a);
		int size = sc.nextInt();
		int[] arr = new int[size];// size af an array initialized;
		for (int i = 0; i < size; i++) {
			arr[i] = sc.nextInt();
		}
		System.out.print("[");
		for (int i = 0; i < size; i++) {
			System.out.print(arr[i]);
			if (i != size - 1) {
				System.out.print(","); // printing quama after the value of array not after the last element
			}
		}
		System.out.println("]");
		int no = sc.nextInt(); // ask user that this no is present in array or not
		boolean flag = false;
		for (int i = 0; i < size; i++) {
			if (arr[i] == no)
				flag = true; // we used the flag here because in for loop sometime we can't put else because
								// every time if is false else is printed so we use flag
		}
		if (flag)
			System.out.println(no + ": is present");
		else
			System.out.println(no + ": is not present");
///////////////////////////////////////////////////////////////////////////////////////////////////////
		// largest and smallest element of the array
		int large = arr[0];
		for (int i = 0; i < size; i++) {
			if (large < arr[i]) {
				large = arr[i];
			}
		}
		System.out.println(large + " :" + "is the largest number.");
		int small = arr[0];
		for (int i = 0; i < size; i++) {
			if (small > arr[i]) {
				small = arr[i];
			}
		}
		System.out.println(small + " :" + "is the smallest number.");// end of smallest and largest code.
		System.out.println("\n");
/////////////////////////////////////////////////////////////////////////////////////////////////////		
		// now copy the arrray in different array but in reverse manner.
		int[] brr = new int[size];
		int b = 0;
		for (int i = size - 1; i >= 0; i--) {
			brr[b] = arr[i];
			b++;
		}
		for (int i = 0; i < size; i++)
			System.out.print(brr[i]); // end of reverse array code
		System.out.println("\n\n");
/////////////////////////////////////////////////////////////////////////////////////////////////////
		// find addition and multiplication of array element.
		int add = arr[0], mul = arr[0];
		for (int i = 1; i < size; i++) {
			add = add + arr[i];
			mul = mul * arr[i];
		}
		System.out.println("add =:" + add + " " + "mul=:" + mul);// end of addition and multiplication
		System.out.println("\n\n");
/////////////////////////////////////////////////////////////////////////////////////////////////////
		// from here we print the all the variables value count from array.
		int countPositive = 0, countNegative = 0, countEven = 0, countOdd = 0, countZero = 0;
		for (int i = 0; i < size; i++) {
			if (arr[i] > 0 && arr[i] % 2 == 0) {
				countEven++;
			} else if (arr[i] % 2 != 0 && arr[i] > 0) {
				countOdd++;
			}
//			if(arr[i] ==0) {
//				countZero++;
//			}
			if (arr[i] < 0)
				countNegative++;
			else if (arr[i] > 0)
				countPositive++;
			else
				countZero++;
		}
		System.out.println("[positive:" + countPositive + "," + "negative:" + countNegative + "," + "even:" + countEven
				+ "," + "odd:" + countOdd + "," + "zero:" + countZero + "]");

		System.out.println("=======Array practice end here======");
		System.out.println("\n\n");
/////////////////////////////////////////////////////////////////////////////////////////////////////	
		System.out.println("=======collection practice========");
		HashSet<String> set = new HashSet<String>();
		set.add("aj");
		set.add("rm");
		set.add("yz");
		set.add("rm");
		set.add("rm");
		Iterator it = set.iterator();
		System.out.println(set);
		Object[] array = set.toArray();
		int i = 0;
		while (it.hasNext()) {
			System.out.println("set values are:" + it.next() + array[i]);
			i++;
		}
		Map<String, HashSet<String>> map = new HashMap<String, HashSet<String>>();
		map.put("one", set);
		map.put("two", set);
		map.put("three", set);
		map.put("four", set);
		Set<String> set1 = map.keySet();
		System.out.println(map.entrySet());
		map.forEach(new BiConsumer<String, HashSet<String>>() {
			@Override
			public void accept(String t, HashSet<String> u) {
				System.out.println("value of t is: " + t + " and the value of U is : " + u);

			}
		});
		map.forEach((y, u) -> {
			System.out.println("value of Y: " + y + " value of U: " + u);
		});
		for (Entry<String, HashSet<String>> entry : map.entrySet()) {
			System.out.println(entry.getKey() + " :" + entry.getValue());
		}
		System.out.println(map.values());
		System.out.println(set1);
		List<String> list = new ArrayList<String>();
		for (int i1 = 0; i1 < 4; i1++) {
			list.add("you are so great");
		}
		list.stream().forEach((s) -> {
			System.out.println("list of String :" + s);
		});

		System.out.println("=======collection practice========");
	}
}
