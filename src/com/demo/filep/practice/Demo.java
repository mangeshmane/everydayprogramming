package com.demo.filep.practice;

import java.util.ArrayList;

public class Demo {

	public static void m1(Object o){
		//display employee data here
		System.out.println(((Employees)o));
	}
	public static void main(String[] args) {
		m1(new Employees(102, "ram", 2000));
		//create Employee object here with 101,sam,1000 data
		//call m1 method by passing this Employee object
		ArrayList<Employees> al = new ArrayList<Employees>();
		al.add(new Employees(102, "ram", 2000));
		al.add(new Employees(103, "shyam", 3000));
		al.add(new Employees(101, "sam", 10000));
		al.add(new Employees(104, "sameer", 40000));
		//sort list in ascending order of salary using sort method and Comparator
		ArrayList<Integer> al1 = new ArrayList<Integer>();
		for(Employees ep:al) {
			if(ep.getSalary()>3000) 
				al1.add(ep.getId());
		}
		for(Integer a:al1) {
			
			System.out.println(a);
		}
	}
}
