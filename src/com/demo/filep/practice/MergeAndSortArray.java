package com.demo.filep.practice;

public class MergeAndSortArray {

	public static void main(String[] args) {
		int[] arr1 = { 1, 5, 4, 3, 6,47,454,464,45 };
		int[] arr2 = { 2, 5, 8, 7,23,4,53,54,7,2 };
		int size = arr1.length + arr2.length;
		int[] array = new int[size];

		for (int i = 0; i < arr1.length; i++) {
			array[i] = arr1[i];                   // 1st array insertion 
			if (array[arr1.length - 1] != 0) {         //here we are inserting the value into the new array from 1st and 2nd array.
				for (int j = 0; j < arr2.length; j++) {
					array[arr1.length + j] = arr2[j];
				}
				System.out.println("you are so clever");
			}
		}
		System.out.println(array[5]);
//		for (int i = 0; i < arr2.length; i++) {  this code is also for inserting the second array with new array.
//			array[arr1.length + i] = arr2[i];   
//		}

		for (int i = 0; i < array.length; i++) {
			for (int j = i; j < array.length; j++) { //this is what we call the sorting technique. in that if we try to copy the any arrya value into another it will override the same value withit 
				if (array[i] > array[j]) {         //that is why we use temp var and store the value and then swap it and then we start the array traversing through the loop and second loop is start with another i value.
					int temp = array[i];    
					array[i] = array[j];
					array[j] = temp;
				}
			}
		}
		int k = 0;
		while (k < array.length) {
			System.out.print(array[k] + " ");
			k++;
		}

	}
}
