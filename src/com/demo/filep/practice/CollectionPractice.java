package com.demo.filep.practice;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Employee {
	int id;
	String name;
	String sal;
	static String company = "tcs";

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", sal=" + sal + "]";
	}

	public void printEmp() {
		System.out.println(Sain.getMap().get(1).toString());
	}
}

class Sain extends Employee {
	private int a;
	String b;
	String c;
	private static HashMap<Integer, Employee> map = new HashMap<Integer, Employee>();

	public static HashMap<Integer, Employee> getMap() {
		return map;
	}

	public static void setMap(HashMap<Integer, Employee> map) {
		Sain.map = map;
	}

	public void read() {
		this.a = 10;
		System.out.println(a);
	}

	@SuppressWarnings("unchecked")
	public Map<Integer, Employee> saveEmployee() {
		Employee emp = new Employee();
		emp.id = 1;
		emp.name = "adidas";
		emp.sal = "39393";
		return (Map<Integer, Employee>) map.put(1, emp);
	}

}

class rain extends Employee {
	int id;
	String name;
	String company;
}

class Palindrome {
	String pal = "abcba";

	@Override
	public String toString() {
		return "Palindrome [pal=" + pal + "]";
	}

	public void checkPalindrome() {

		int pl = 12321;
		int temp = pl;
		int ser = 0;
		for (int i = 0; pl > 0; i++) {
			ser = ser * 10 + pl % 10;
			pl = pl / 10;
		}
		if (temp == ser) {
			System.out.println(temp + "is a palindrome number");
		} else {
			System.out.println(temp + "is not palindrome number");
		}
		
	}
}

class CollectionPractice {
	int a = 20;
	public static void main(String[] args) {
		Map<String,String> hash = new HashMap<String,String>();
		List<String> list = new ArrayList<String>();
		list.add("One");
	//	list.add("Two");
		hash.put("one", "{auth = user}");
		String h = hash.get("one");
		h.toString();
		System.out.println(h.split("=")[1].replace("}", "").trim());
		System.out.println(hash);
		
		CollectionPractice ca = new CollectionPractice();
		System.out.println(ca.a);
		LocalDateTime d = LocalDateTime.now();
		System.out.println(d.toString());
		int expiryDate =120;
		System.out.println(expiryDate +"min");
		
		// palindrome code here
		Palindrome palin = new Palindrome();
		palin.checkPalindrome();
		char[] rev = palin.pal.toCharArray();
		int[] num = { 1, 2, 3, 4 };
		System.out.println(rev);
		char[] reverse = new char[rev.length];
		int j = 0;
		System.out.println(rev.length);
		for (int i = rev.length - 1; i >= 0; i--) {
			reverse[j] = rev[i];
			System.out.println(reverse[j]);
			j++;
		}
		if (new String(reverse).equals(new String(rev))) {
			System.out.println("both are equal");
		}

		int ab = 234567;
		int c = 0;
		for (int i = 0; ab > 0; i++) {
			c = c * 10 + ab % 10;
			ab = ab / 10;
		}
		System.out.println("reverse of number ab is " + " " + c);

		Map<Integer, String> map = new HashMap<Integer, String>();
		map.put(1, "hey_you");
		map.put(2, "what's_your_name");
		map.put(3, "ezikio");
		map.put(4, "f**k_you_ezikio");
		map.put(5, "and_what's_your_name");
		for (Map.Entry<Integer, String> e : map.entrySet()) {
			System.out.println(e.getKey() + " " + e.getValue());

		}

		int[] arr = { 9, 5, 8, 6, 4, 2, 1 };
		for (int i = arr.length - 1; i > 0; i--) {
			System.out.println(arr[i]);
		}

		Sain cp = new Sain();
		cp.saveEmployee();
		cp.printEmp();
	}
}