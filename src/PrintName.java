import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Scanner; 

// first two classes using the extending the thread class
class A extends Thread {
	public void run() {
		for (int i = 0; i <= 4; i++) {
			System.out.println("hey");
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
			} // if we implement thread we need to extends
				// it from Thread class or Runnable interface
		}
	}
}

class B extends Thread {
	public void run() {
		for (int i = 0; i <= 4; i++) {
			System.out.println("babe");
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
			} // also we need to run the thread using calling thread.start() method
		} // and also override the run method in it so we can print the output.
	}
}

// And these two classes for using Runnable interface
class C implements Runnable {
	public void run() {
		for (int i = 0; i < 4; i++) {
			System.out.println("hi runnable");
			try{Thread.sleep(1000);}catch(Exception e) {};
		}
	}
}

class D implements Runnable {

	public void run() {
		for (int i = 0; i < 4; i++) {
			System.out.println("hello runnable");
			try{Thread.sleep(1000);}catch(Exception e) {};
		}
	}
}




class Fabonnaci{
	public void fabonacciNumberStream(int a) {
		int first= 0;int sec=1;
		for(int k=1;k<=a;k++) {
			int third = 0;
			System.out.println(first);
			third= first+sec;
			first = sec;       //0,1,1,2,3,5,8,13..... c=a+b;
			sec = third;
		}
	}
	public int fabonaciRecursion(int a) {
		if(a<=1)return a;
		System.out.println(a);
		return fabonaciRecursion(a-1)+fabonaciRecursion(a-2);
	}
}

class SumOfNamturalNo{
	public void sumOfNaturalNo() {
		int i= 1000;
		int c = 0;
		for(int j= 1;j<i;j++) {
			c+=j; 
		}
		System.out.println("sum of natural numbers from 0 to 1000:"+c);	
	}
}
class Factorial{
	public void findFactorial() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter your number:");
		int num = sc.nextInt();
		int f = 1;
		for(int i=1;i<=num;i++) {
			f = f * i;
		}
		System.out.println("factorial of num is : "+f);
	}
}
class EvenOdd{
	public void evenOdd() {  // find even and Odd numbers between 0 to 1000
		for(int i = 0;i<10;i++) {
			if(i%2 == 0 || i == 0) {
				System.out.println("number is even: "+i);
			}
			else {
				System.out.println("number is odd: "+ i);
			}
		}
	}
}
class Scan{
	public void nextIntLine() {
		Scanner sc = new Scanner(System.in);
		int[][] a = new int[3][5];
		for(int i = 0;i<3;i++) {
			for(int j =0;j<5;j++) {
				System.out.println("please enter your matrix");
				a[i][j]=sc.nextInt();
			}
		}
		for(int i = 0;i<3;i++) {
			for(int j =0;j<5;j++) {
				System.out.print(a[i][j]+" ");
			}
			System.out.println(" ");
		}
		System.out.println("Enter your number:");
		System.out.println(sc.nextInt());
		sc.nextLine(); // if any character with \n (new line) with end it return until it wait for new line 
		System.out.println("enter another itn:"+sc.nextInt());
		//example :
		System.out.println("enter your number:");
		int as = sc.nextInt(); //if i give input as 23 and give space nextInt is wait till it get the int value after that it return and other will stay in the input buffer.
		System.out.println(as);// so any input character in nextInt take value in the form of 343\n means after 343 it return and \n is go inside the buffer.
		String sss = sc.nextLine(); // so if we write another nextInt so it take the value inside the input buffer but if space contain it doesn't take but nextLine will take.
		System.out.println(sss+ sss.length());
		System.out.println(as+"          "+as);
		
	}
}
public class PrintName {

	public static void main(String[] args) throws ParseException {
		
//2.Decimal format with precision and with adding locales.us and ,between decimal.
		double d = 23223.32d;
		String s1 = String.format(Locale.US, "%.1f", d);
		System.out.println(s1);
		NumberFormat dff = NumberFormat.getCurrencyInstance(Locale.US);
		dff.format(d);
		Scanner scanner = new Scanner(System.in);
        double payment = scanner.nextDouble();
        scanner.close();
        NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
        String us = n.format(payment);
       NumberFormat n1 = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
        String india = n1.format(payment);
        NumberFormat n2 = NumberFormat.getCurrencyInstance(Locale.CHINA);
        String china = n2.format(payment);
        NumberFormat n3 = NumberFormat.getCurrencyInstance(Locale.FRANCE);
        String france = n3.format(payment);
        
        // Write your code here.
        
        System.out.println("US: " + us);
        System.out.println("India: " + india);
        System.out.println("China: " + china);
        System.out.println("France: " + france);
        System.out.println(dff.toString());
		
		
		
		
		
		
		
		
//1. Date format code on hackerrank		
		SimpleDateFormat df = new SimpleDateFormat("YYYY MM DD");
		
		LocalDate localDate = LocalDate.of(2021, 1, 18);
		localDate.getDayOfWeek();
		Date datea = new GregorianCalendar(2021, 01 - 1, 18).getTime();

		String date = df.format(datea);
		Date datee  = df.parse(date);
		System.out.println(date+" "+datea.getDay()+" "+localDate.getDayOfWeek().toString());
		String[] arr = { "Sunday", "Monday","Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
		for(int i = 0;i<arr.length;i++) {
			if(datea.getDay() == i) {
				System.out.println(arr[i].toUpperCase());
			}
			
		}
		
		Scan s = new Scan();
		s.nextIntLine();
//		A a = new A(); // so basically we need to override the run method in A and B class because
//						// start() method internally call run method to run thread
//		B b = new B();
//		a.start();
//		try {
//			Thread.sleep(1000);
//		} catch (Exception e) {
//		}
//		b.start();
//		
//		Runnable c = new C();
//		Runnable d = new D();
//		Thread t1 = new Thread(new C());
//		Thread t2 = new Thread(new D());
//		t1.start();
//		try{Thread.sleep(10);}catch(Exception e) {};
//		t2.start();
//
		int a= 2534;
		int b = 4;
		System.out.println(a&b);
		
		int i=5,j = 5,k;
		i = i++ + ++i + ++i;
		 k= j + j++;
		System.out.println("i:"+i+"j"+j+" "+k);
		
		int count= 0;
		int rev = 0;
		int o = a%10;      
		System.out.println(o);
		while(a!=0) { 
			int no = a%10;
			System.out.println(no);
			rev = rev * 10 +no; 
			a/=10;
			count++;
		}
		System.out.println("count: "+ count+" "+ "reversed number: "+rev);
		Fabonnaci f = new Fabonnaci();
		//f.fabonacciNumberStream(10);
		System.out.println(f.fabonaciRecursion(5));
		SumOfNamturalNo sn = new SumOfNamturalNo();
		sn.sumOfNaturalNo();
		Factorial fact = new Factorial();
		fact.findFactorial();
		EvenOdd eo = new EvenOdd();
		eo.evenOdd();
	}
}